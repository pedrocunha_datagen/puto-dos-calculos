﻿
namespace Puto_dos_Cálculos_3
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btncomecar = new System.Windows.Forms.Button();
            this.lblconta = new System.Windows.Forms.Label();
            this.txtbox = new System.Windows.Forms.TextBox();
            this.btnproxima = new System.Windows.Forms.Button();
            this.btnportugues = new System.Windows.Forms.Button();
            this.btnenglish = new System.Windows.Forms.Button();
            this.lblrecorde = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.lblpontuacao = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btncomecar
            // 
            this.btncomecar.Font = new System.Drawing.Font("Unispace", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncomecar.Location = new System.Drawing.Point(174, 382);
            this.btncomecar.Name = "btncomecar";
            this.btncomecar.Size = new System.Drawing.Size(145, 45);
            this.btncomecar.TabIndex = 4;
            this.btncomecar.Text = "COMEÇAR";
            this.btncomecar.UseVisualStyleBackColor = true;
            this.btncomecar.Click += new System.EventHandler(this.btncomecar_Click);
            // 
            // lblconta
            // 
            this.lblconta.AutoSize = true;
            this.lblconta.BackColor = System.Drawing.Color.Black;
            this.lblconta.Font = new System.Drawing.Font("Unispace", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblconta.ForeColor = System.Drawing.Color.White;
            this.lblconta.Location = new System.Drawing.Point(224, 324);
            this.lblconta.Name = "lblconta";
            this.lblconta.Size = new System.Drawing.Size(346, 23);
            this.lblconta.TabIndex = 5;
            this.lblconta.Text = "Olá, sou o Puto dos Cálculos";
            // 
            // txtbox
            // 
            this.txtbox.Font = new System.Drawing.Font("Unispace", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox.Location = new System.Drawing.Point(197, 350);
            this.txtbox.Name = "txtbox";
            this.txtbox.Size = new System.Drawing.Size(98, 30);
            this.txtbox.TabIndex = 6;
            this.txtbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtbox.Visible = false;
            this.txtbox.TextChanged += new System.EventHandler(this.txtbox_TextChanged);
            this.txtbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtbox_KeyDown);
            // 
            // btnproxima
            // 
            this.btnproxima.Font = new System.Drawing.Font("Unispace", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnproxima.Location = new System.Drawing.Point(174, 423);
            this.btnproxima.Name = "btnproxima";
            this.btnproxima.Size = new System.Drawing.Size(145, 45);
            this.btnproxima.TabIndex = 7;
            this.btnproxima.Text = "PRÓXIMA";
            this.btnproxima.UseVisualStyleBackColor = true;
            this.btnproxima.Visible = false;
            this.btnproxima.Click += new System.EventHandler(this.btnproxima_Click);
            // 
            // btnportugues
            // 
            this.btnportugues.Font = new System.Drawing.Font("Unispace", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnportugues.Location = new System.Drawing.Point(12, 382);
            this.btnportugues.Name = "btnportugues";
            this.btnportugues.Size = new System.Drawing.Size(145, 45);
            this.btnportugues.TabIndex = 8;
            this.btnportugues.Text = "Português";
            this.btnportugues.UseVisualStyleBackColor = true;
            this.btnportugues.Click += new System.EventHandler(this.btnportugues_Click);
            // 
            // btnenglish
            // 
            this.btnenglish.Font = new System.Drawing.Font("Unispace", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnenglish.Location = new System.Drawing.Point(327, 382);
            this.btnenglish.Name = "btnenglish";
            this.btnenglish.Size = new System.Drawing.Size(145, 45);
            this.btnenglish.TabIndex = 9;
            this.btnenglish.Text = "English";
            this.btnenglish.UseVisualStyleBackColor = true;
            this.btnenglish.Click += new System.EventHandler(this.btnenglish_Click);
            // 
            // lblrecorde
            // 
            this.lblrecorde.AutoSize = true;
            this.lblrecorde.BackColor = System.Drawing.Color.Black;
            this.lblrecorde.Font = new System.Drawing.Font("Unispace", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrecorde.ForeColor = System.Drawing.Color.White;
            this.lblrecorde.Location = new System.Drawing.Point(102, 22);
            this.lblrecorde.Name = "lblrecorde";
            this.lblrecorde.Size = new System.Drawing.Size(118, 23);
            this.lblrecorde.TabIndex = 10;
            this.lblrecorde.Text = "Recorde:0";
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // lblpontuacao
            // 
            this.lblpontuacao.AutoSize = true;
            this.lblpontuacao.BackColor = System.Drawing.Color.Black;
            this.lblpontuacao.Font = new System.Drawing.Font("Unispace", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpontuacao.ForeColor = System.Drawing.Color.White;
            this.lblpontuacao.Location = new System.Drawing.Point(262, 22);
            this.lblpontuacao.Name = "lblpontuacao";
            this.lblpontuacao.Size = new System.Drawing.Size(142, 23);
            this.lblpontuacao.TabIndex = 11;
            this.lblpontuacao.Text = "Pontuação:0";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Puto_dos_Cálculos_3.Properties.Resources.happy;
            this.pictureBox1.Location = new System.Drawing.Point(118, 62);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(250, 250);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.lblpontuacao);
            this.Controls.Add(this.lblrecorde);
            this.Controls.Add(this.btnenglish);
            this.Controls.Add(this.btnportugues);
            this.Controls.Add(this.btnproxima);
            this.Controls.Add(this.txtbox);
            this.Controls.Add(this.lblconta);
            this.Controls.Add(this.btncomecar);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Puto dos Cálculos";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btncomecar;
        private System.Windows.Forms.Label lblconta;
        private System.Windows.Forms.TextBox txtbox;
        private System.Windows.Forms.Button btnproxima;
        private System.Windows.Forms.Button btnportugues;
        private System.Windows.Forms.Button btnenglish;
        private System.Windows.Forms.Label lblrecorde;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label lblpontuacao;
    }
}

