﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Puto_dos_Cálculos_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static int resultado;                           // resultado num1+operador+num2
        public static int resposta;                            // resposta dada pelo utilizador


        public static Random numero = new Random();            // Números e operador (só para a primeira pergunta)

        public static int num1 = numero.Next(1, 15);     // Número 1
        public static int num2 = numero.Next(0, 15);     // Número 2
        public static int operador = numero.Next(1, 10); // Operador (soma ou multiplicação)

        public static int pt = 1;
        public static int en = 0;
        public static int recorde;
        public static int pontuacao = 0;

        public static int ticks;



        private void Form1_Load(object sender, EventArgs e)     // Posições dos objetos
        {
            pictureBox1.Left = (this.ClientSize.Width - pictureBox1.Width) / 2;
            pictureBox1.Top = (this.ClientSize.Height - pictureBox1.Height) / 2 - (this.ClientSize.Height) / 6;

            btncomecar.Left = (this.ClientSize.Width - btncomecar.Width) / 2;
            btncomecar.Top = (this.ClientSize.Height - btncomecar.Height) / 2 + (this.ClientSize.Height) / 3;

            lblconta.Left = (this.ClientSize.Width - lblconta.Width) / 2;
            lblconta.Top = (this.ClientSize.Height - lblconta.Height) / 2 + (this.ClientSize.Height) / 6;

            btnproxima.Left = (this.ClientSize.Width - btnproxima.Width) / 2;
            btnproxima.Top = (this.ClientSize.Height - btnproxima.Height) / 2 + (this.ClientSize.Height) / 3;

            txtbox.Left = (this.ClientSize.Width - txtbox.Width) / 2;
            txtbox.Top = (this.ClientSize.Height - txtbox.Height) / 2 + (this.ClientSize.Height) / 4 - (this.ClientSize.Height) / 50;

            btnportugues.Left = (this.ClientSize.Width - btnportugues.Width) / 2 - (this.ClientSize.Width) / 3;
            btnportugues.Top = (this.ClientSize.Height - btnportugues.Height) / 2 + (this.ClientSize.Height) / 3;

            btnenglish.Left = (this.ClientSize.Width - btnenglish.Width) / 2 + (this.ClientSize.Width) / 3;
            btnenglish.Top = (this.ClientSize.Height - btnenglish.Height) / 2 + (this.ClientSize.Height) / 3;

            lblrecorde.Left = (this.ClientSize.Width - lblrecorde.Width) / 2 - (this.ClientSize.Width) / 3;
            lblrecorde.Top = (this.ClientSize.Height - lblrecorde.Height) / 2 - (this.ClientSize.Height) / 3 - (this.ClientSize.Height) / 10;

            lblpontuacao.Left = (this.ClientSize.Width - lblpontuacao.Width) / 2 + (this.ClientSize.Width) / 3;
            lblpontuacao.Top=(this.ClientSize.Height-lblpontuacao.Height)/ 2 - (this.ClientSize.Height) / 3 - (this.ClientSize.Height) / 10;



            System.IO.Stream str = Properties.Resources.musicahappy;     //música ambiente
            System.Media.SoundPlayer snd = new System.Media.SoundPlayer(str);
            snd.PlayLooping();
        }

        private void HighScore()
        {
            /*
            string FilePath = @"C:\Users\Desktop\HighScore.txt";
            if(File.Exists(FilePath))
            {
                StreamReader Sr = new StreamReader(FilePath);
                Sr.ReadToEnd();
            }
            */
        }

        private void btncomecar_Click(object sender, EventArgs e)
        {
            timer2.Start();
            ticks = 0;
            btnportugues.Hide();
            btnenglish.Hide();
            btncomecar.Hide();
            btnproxima.Show();
            txtbox.Show();
            this.ActiveControl = txtbox;
            txtbox.Focus();
            if (operador <= 5)
            {
                lblconta.Text = Convert.ToString(num1) + "+" + Convert.ToString(num2);
                lblconta.Left = (this.ClientSize.Width - lblconta.Width) / 2;
            }
            else if (operador >= 6)
            {
                lblconta.Text = Convert.ToString(num1) + "*" + Convert.ToString(num2);
                lblconta.Left = (this.ClientSize.Width - lblconta.Width) / 2;
            }
        }

        private void btnproxima_Click(object sender, EventArgs e)
        {
            ticks = 0;
            resposta = Convert.ToInt32(txtbox.Text);

            if (operador < 6)
            {
                resultado = num1 + num2;
            }
            else if (operador > 5)
            {
                resultado = num1 * num2;
            }


            if (resposta == resultado)
            {
                pontuacao++;
                if (pt == 1 && en == 0)
                {
                    lblpontuacao.Text = "Pontuação:" + Convert.ToString(pontuacao);

                    lblrecorde.Left = (this.ClientSize.Width - lblrecorde.Width) / 2 - (this.ClientSize.Width) / 3;
                    lblrecorde.Top = (this.ClientSize.Height - lblrecorde.Height) / 2 - (this.ClientSize.Height) / 3 - (this.ClientSize.Height) / 10;

                    lblpontuacao.Left = (this.ClientSize.Width - lblpontuacao.Width) / 2 + (this.ClientSize.Width) / 3;
                    lblpontuacao.Top = (this.ClientSize.Height - lblpontuacao.Height) / 2 - (this.ClientSize.Height) / 3 - (this.ClientSize.Height) / 10;

                    if (pontuacao >= recorde)
                    {
                        recorde = pontuacao;
                        lblrecorde.Text = "Recorde:" + Convert.ToString(recorde);
                        lblrecorde.Left = (this.ClientSize.Width - lblrecorde.Width) / 2 - (this.ClientSize.Width) / 3;
                    }
                }
                else if (pt == 0 && en == 1)
                {
                    lblpontuacao.Text = "Score:" + Convert.ToString(pontuacao);

                    lblrecorde.Left = (this.ClientSize.Width - lblrecorde.Width) / 2 - (this.ClientSize.Width) / 3;
                    lblrecorde.Top = (this.ClientSize.Height - lblrecorde.Height) / 2 - (this.ClientSize.Height) / 3 - (this.ClientSize.Height) / 10;

                    lblpontuacao.Left = (this.ClientSize.Width - lblpontuacao.Width) / 2 + (this.ClientSize.Width) / 3;
                    lblpontuacao.Top = (this.ClientSize.Height - lblpontuacao.Height) / 2 - (this.ClientSize.Height) / 3 - (this.ClientSize.Height) / 10;

                    if (pontuacao >= recorde)
                    {
                        recorde = pontuacao;
                        lblrecorde.Text = "Recorde:" + Convert.ToString(recorde);
                        lblrecorde.Left = (this.ClientSize.Width - lblrecorde.Width) / 2 - (this.ClientSize.Width) / 3;
                    }
                }
                txtbox.Clear();
                num1 = numero.Next(1, 15);
                num2 = numero.Next(0, 15);
                operador = numero.Next(1, 10);

                if (operador <= 5)
                {
                    lblconta.Text = Convert.ToString(num1) + "+" + Convert.ToString(num2); // nova pergunta
                    lblconta.Left = (this.ClientSize.Width - lblconta.Width) / 2;
                }
                else if (operador >= 6)
                {
                    lblconta.Text = Convert.ToString(num1) + "*" + Convert.ToString(num2); // nova pergunta
                    lblconta.Left = (this.ClientSize.Width - lblconta.Width) / 2;
                }
            }

            else
            {

                System.IO.Stream str = Properties.Resources.fail;
                System.Media.SoundPlayer snd = new System.Media.SoundPlayer(str);
                snd.Play();

                pictureBox1.Image = Properties.Resources.angry;
                pictureBox1.Size = new System.Drawing.Size(500, 500);
                pictureBox1.Left = (this.ClientSize.Width - pictureBox1.Width) / 2;
                pictureBox1.Top = (this.ClientSize.Height - pictureBox1.Height) / 2;
                btnproxima.Hide();
                txtbox.Hide();
                if (pt == 1 && en == 0)
                {
                    lblconta.Text = ("ERRADO!");
                }
                else if (pt == 0 && en == 1)
                {
                    lblconta.Text = ("WRONG!");
                }
                lblconta.Left = (this.ClientSize.Width - lblconta.Width) / 2;
                /*
                var original = this.Location;
                var rnd = new Random(1337);
                const int shake_amplitude = 10;
                for (int i = 0; i < 150; i++)
                {
                    this.Location = new Point(original.X + rnd.Next(-shake_amplitude, shake_amplitude), original.Y + rnd.Next(-shake_amplitude, shake_amplitude));
                    System.Threading.Thread.Sleep(20);
                    this.Location = original;
                }
                */
                Application.DoEvents();
                onError(this);
                Application.Exit();

            }
        }



        // https://www.youtube.com/watch?v=qQTv3jRBlSY&t=21s


        private static void onError(Form form)
        {
            var original = form.Location;
            var rnd = new Random(1337);
            const int shake_amplitude = 5;
            for (int i = 0; i < 150; i++)
            {
                form.Location = new Point(original.X + rnd.Next(-shake_amplitude, shake_amplitude), original.Y + rnd.Next(-shake_amplitude, shake_amplitude));
                System.Threading.Thread.Sleep(20);
                form.Location = original;
            }
        }

        private void btnportugues_Click(object sender, EventArgs e)
        {
            pt = 1;
            en = 0;
            lblpontuacao.Text = "Pontuação:" + Convert.ToString(pontuacao);
            lblrecorde.Text = "Recorde:" + Convert.ToString(recorde);
            lblrecorde.Left = (this.ClientSize.Width - lblrecorde.Width) / 2 - (this.ClientSize.Width) / 3;
            this.Text = "Puto dos Cálculos";
            lblconta.Text = "Olá, sou o Puto dos Cálculos";
            lblconta.Left = (this.ClientSize.Width - lblconta.Width) / 2;
            btnproxima.Text = "Próxima";
            btncomecar.Text = "Começar";
        }

        private void btnenglish_Click(object sender, EventArgs e)
        {
            pt = 1;
            en = 0;
            lblpontuacao.Text = "Score:" + Convert.ToString(pontuacao);
            lblrecorde.Text = "Highscore:" + Convert.ToString(recorde);
            lblrecorde.Left = (this.ClientSize.Width - lblrecorde.Width) / 2 - (this.ClientSize.Width) / 3;
            this.Text = "Maths Kid";
            lblconta.Text = "Hello, I'm the Maths Kid";
            lblconta.Left = (this.ClientSize.Width - lblconta.Width) / 2;
            btnproxima.Text = "Next";
            btncomecar.Text = "Start";
        }

        private void txtbox_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnproxima.PerformClick();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            ticks++;
            if(ticks==20)
            {
                timer2.Stop();
                System.IO.Stream str = Properties.Resources.fail;
                System.Media.SoundPlayer snd = new System.Media.SoundPlayer(str);
                snd.Play();

                pictureBox1.Image = Properties.Resources.angry;
                pictureBox1.Size = new System.Drawing.Size(500, 500);
                pictureBox1.Left = (this.ClientSize.Width - pictureBox1.Width) / 2;
                pictureBox1.Top = (this.ClientSize.Height - pictureBox1.Height) / 2;
                btnproxima.Hide();
                txtbox.Hide();
                if (pt == 1 && en == 0)
                {
                    lblconta.Text = ("TARDE DEMAIS!");
                }
                else if (pt == 0 && en == 1)
                {
                    lblconta.Text = ("TOO LATE!");
                }
                lblconta.Left = (this.ClientSize.Width - lblconta.Width) / 2;
                Application.DoEvents();
                onError(this);
                Application.Exit();
            }

            
        }
    }
}